## 2.1.0-rc.0

- 适配ComponentV2装饰器

## 2.0.0

1.DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
2.ArkTs 语法适配

## 1.0.4

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）。

## v1.0.3

- api8到api9的切换

## v1.0.0

1. 实现Text自动调整文本大小并自适应其边界的功能。